import React from "react";

// Styled Components
import {DotWrapper, Dot} from './styled/LoadingStyles'

const Loading = () => {
    return (
      <DotWrapper>
        <Dot delay=".1s" />
        <Dot delay=".2s" />
        <Dot delay=".3s" />
      </DotWrapper>
    )
}

export default Loading