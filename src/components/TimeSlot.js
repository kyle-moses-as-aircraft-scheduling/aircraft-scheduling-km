import React from 'react';
import PropTypes from 'prop-types';
import { FlightTimeSlot, TurnaroundTime, IdleTime } from './styled/TimelineStyles'


function Timeslot({timeSlotWidth, idleTimeWidth}) {
	return (
		<>
			
			<IdleTime idleTimeWidth={idleTimeWidth} />
			<FlightTimeSlot timeSlotWidth={timeSlotWidth}></FlightTimeSlot>
			<TurnaroundTime />
		</>
	)
}

Timeslot.propTypes = {
	timeSlotWidth: PropTypes.number
}

export default Timeslot

