import React from 'react';
import PropTypes from 'prop-types';
import TimeSlot from './TimeSlot';
import {TimelineContainer} from './styled/TimelineStyles'
function Timeline({rotationList}) {
	return (
		<TimelineContainer>
			{rotationList.map((flight, index, flights) => {
				let previousFlight = flights[index-1];
				return <TimeSlot 
					key={flight.id} 
					timeSlotWidth={100*((flight.arrivaltime - flight.departuretime) / 86400)} 
					idleTimeWidth={index >= 1 ? (100*((flight.departuretime - previousFlight.arrivaltime) / 86400)) : 0} 
				/>
			})}
		</TimelineContainer>
	)
};

Timeline.propTypes = {
	rotationList: PropTypes.array
};

export default Timeline

