import React from 'react';
import PropTypes from 'prop-types';

// Styled Components
import {FlightContainer, RemoveFlight, AddToFlightPlan} from './styled/FlightListStyles';

function Flight({id, isLast, origin, destination, readable_arrival, readable_departure, isRotation, addFlight, currentDestination, removeFlight}) {
	const handleAdd = () => {
		if (currentDestination === undefined || currentDestination === origin ) {
			addFlight(id)
		}
	}
	return (
		<FlightContainer isRotation={isRotation} currentDestination={currentDestination} origin={origin}>
			{ isRotation ?
				isLast ? <RemoveFlight onClick={() => removeFlight(id)} /> : ''
				: 
				<AddToFlightPlan 
					currentDestination={currentDestination} 
					origin={origin} 
					onClick={handleAdd}
				/>
			}
			<p>{id}</p>
			<div className="origin">{origin} <br />{readable_departure}</div>
			<div className="destination">{destination} <br />{readable_arrival}</div>
		</FlightContainer>
	)
}

Flight.propTypes = {
	flight: PropTypes.object,
	isRotation: PropTypes.bool, 
	addFlight: PropTypes.func, 
	currentDestination: PropTypes.string, 
	removeFlight: PropTypes.func
}

export default Flight

