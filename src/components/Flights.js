
import PropTypes from 'prop-types';
import Flight from './Flight';
import Loading from './Loading';

// Styled Components
import {FlightsContainer, FlightsList} from './styled/FlightListStyles';

function Flights({flightList, addFlight, currentDestination}) {
	return (
		<FlightsContainer>
			<h2>Flights</h2>
			{flightList !== undefined && flightList.length > 0 ? 
			<FlightsList>
				{flightList.map((flight, index, array)=> (
					<Flight key={flight.id} {...flight} isRotation={false} currentDestination={currentDestination} addFlight={addFlight}/>
				))}
			</FlightsList>
			:
				<Loading />
			}
		</FlightsContainer>
	)
}

Flights.propTypes = {
	flightsList: PropTypes.array,
	addFlight: PropTypes.func,
	currentDestination: PropTypes.string
}

export default Flights
