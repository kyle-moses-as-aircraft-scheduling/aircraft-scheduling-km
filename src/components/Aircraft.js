import PropTypes from 'prop-types';

// Styled Components
import {AircraftContainer} from './styled/AircraftsStyles';

function Aircraft({ident, bookPercent}) {
	return (
		<AircraftContainer>
			<p>{ident}</p>
			<p>({bookPercent}%)</p>
		</AircraftContainer>
	)
}

Aircraft.propTypes = {
	ident: PropTypes.string,
	bookPercent: PropTypes.string,
}

export default Aircraft
