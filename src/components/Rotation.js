
import PropTypes from 'prop-types';
import Flight from './Flight'
import TimeLine from './Timeline';
import {RotationsContainer} from './styled/RotationsStyles'

function Rotations({rotationList, removeFlight}) {
	return (
		<RotationsContainer>
			<div className="rotationListContainer">
				<h2>Rotation</h2>
				{rotationList.length > 0 ? 
					<ul className="rotationList">
						{rotationList.map((rotation, index, array)=> (
							<Flight 
								key={rotation.id}  
								isRotation={true}  
								isLast={index >= array.length - 1 ? true : false}
								removeFlight={removeFlight}
								{...rotation}
							/>
						))}
					</ul> 
				: 
					<p className="notice">Please select a flight to start your rotation</p>
				}
				{ rotationList.length > 0 ? 
					<p className="notice">Please choose the next flight with an <strong>Origin</strong> matching the current flights <strong>Destination</strong> to maximize efficiency!</p>
				:''}
				
			</div>
			<TimeLine rotationList={rotationList}/>
		</RotationsContainer>
	)
}

Rotations.propTypes = {
	rotationList: PropTypes.array,
	removeFlight: PropTypes.func
}

export default Rotations
