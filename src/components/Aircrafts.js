
import PropTypes from 'prop-types';
import Aircraft from './Aircraft';
import Loading from './Loading';
import { calculateBookPercent } from '../lib/utils';

// Styled Components
import { AircraftsContainer, AircraftsList} from './styled/AircraftsStyles';


function Aircrafts({ aircraftsList, rotationList }) {
	return (
		<AircraftsContainer>
			<h2>Aircrafts</h2>
			{aircraftsList !== undefined && aircraftsList.length > 0 ? 
				<AircraftsList>
					{aircraftsList.map((aircraft)=> (
						<Aircraft key={aircraft.ident} ident={aircraft.ident} bookPercent={calculateBookPercent(rotationList)}/>
					))}
				</AircraftsList>
			:
				<Loading />
			}
		</AircraftsContainer>
	)
}

Aircrafts.propTypes = {
	aircraftsList: PropTypes.array,
	rotationList: PropTypes.array,
}

export default Aircrafts
