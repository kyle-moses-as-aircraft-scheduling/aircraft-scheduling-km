import styled from 'styled-components';

export const Header = styled.header`
	display: block;
	width: 900px;
	margin: 60px auto 0;
	align-items: center;
	justify-content: center;
	font-size: calc(10px + 2vmin);
	text-align: center;
	
	&:before {
		
	}
	
	&:after {

	}
`;

export const FlightScheduler = styled.div`
	text-align: center;
	display: flex;
	flex-wrap: wrap;
	flex: 1 1 0;
	width: 900px;
	height: 600px;
	margin: 20px auto;
	overflow:hidden;

	* {
		box-sizing:border-box;
	}

	> div {
		min-height: 0;
		min-width: 0;
		max-height: 100%;
		overflow:hidden;
	}
`;
