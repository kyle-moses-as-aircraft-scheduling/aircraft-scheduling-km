
import styled from 'styled-components';

export const TimelineContainer = styled.div`
	box-sizing: border-box;
	display: flex;
	flex-direction: row;
	height: 60px;
	margin: 25px 5px 0px 5px;
	border: 2px dashed #333;
	position:relative;
	background-color: #ccc;

	&:before {
		content: "|-- 00:00";
		position: absolute;
		top: -25px;
	}
	
	&:after {
		content: "24:00 --|";
		position: absolute;
		top: -25px;
		right: 0;
	}
`

export const FlightTimeSlot = styled.div`
	background-color: green;
	width:${timeSlotWidth => (timeSlotWidth.timeSlotWidth + '%')};
	border-radius: 3px;
`
export const TurnaroundTime = styled.div`
	background-color: purple;
	border-radius: 3px;
	width: 1.38%; // based on 20 minutes of 24 hours
`
export const IdleTime = styled.div`
background-color: gray;
border-radius: 3px;
width: ${idleTimeWidth => (idleTimeWidth.idleTimeWidth + '%')};; // based on 20 minutes of 24 hours
`
