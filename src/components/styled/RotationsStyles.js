import styled from 'styled-components';

export const RotationsContainer = styled.div`
	display: flex;
	flex: 1 1 0;
	flex-direction: column;
	flex-wrap: wrap;
	width: 40%;

	.rotationListContainer {
		box-sizing:border-box;
		padding:10px;
		margin: 0 5px;
		min-height: 0;
		flex: 1 1 0;
		border:1px solid #ccc;
		background-color: #efefef;
	}
	.rotationList {
		list-style: none;
		margin:0;
		padding:0;
		height: 380px;
		overflow-y: scroll;
	}
	.notice {
		color: grey;
		font-size: 0.8rem;
	}
`

 