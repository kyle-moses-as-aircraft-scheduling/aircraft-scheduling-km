
import styled from 'styled-components';
import {BookAdd} from '@styled-icons/boxicons-regular/BookAdd';
import {RemoveCircle} from '@styled-icons/ionicons-sharp/RemoveCircle';

export const FlightContainer = styled.li`
	position: relative;
	width: 100%;
	box-sizing: border-box;
	padding: ${props => (props.isRotation ? '10px 60px 10px 10px' : '10px 10px 10px 60px')};
	border: 1px solid lightgrey;
	background-color: ${props => (props.currentDestination === undefined || props.currentDestination === props.origin ? '#ffffff' : '#cccccc')};
	color: ${props => (props.currentDestination === undefined || props.currentDestination === props.origin ? '' : '#ababab')};
	
	p {
		font-weight: bold;
		margin: 0 0 10px 0;
	}
	.origin {
		display: inline-block;
		width: 50%;
		text-align: left;
	}
	.destination {
		display: inline-block;
		width: 50%;
		text-align: right;
	}

`;

export const AddToFlightPlan = styled(BookAdd)`
	color: ${props => (props.currentDestination === undefined || props.currentDestination === props.origin ? 'green' : '#ababab')};
	position: absolute;
	left: 10px;
	max-width: 30px;
	top: 50%;
	transform: translate(0, -50%);
	cursor: pointer;
`;

export const RemoveFlight = styled(RemoveCircle)`
	color: Red;
	position: absolute;
	right: 10px;
	max-width: 30px;
	top: 50%;
	transform: translate(0, -50%);
	cursor: pointer;
`;

export const FlightsContainer = styled.div`
	width: 40%;
	box-sizing:border-box;
	padding:10px;
	border:1px solid #ccc;
	display: flex;
	background-color: #efefef;
	flex-direction: column;
`;

export const FlightsList = styled.ul`
	overflow-y:scroll;
	list-style: none;
	margin: 0;
	padding: 0;
	flex-grow: 1;
	height: 100%;
`;