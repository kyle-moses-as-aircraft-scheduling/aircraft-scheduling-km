import styled from 'styled-components';

export const AircraftsContainer = styled.div`
	width: 20%;
	box-sizing:border-box;
	padding:10px;
	border:1px solid #ccc;
	background-color: #efefef;
`;

export const AircraftContainer = styled.li`
	position: relative;
	width: 100%;
	box-sizing: border-box;
	padding: 10px 10px 10px 10px;
	border:1px solid lightgrey;
	background: #ffffff;

	p {
		color: #333;
		font-weight: bold;
		margin: 0 0 10px 0;
	}
	.origin {
		display: inline-block;
		width: 50%;
		text-align: left;
	}
	.destination {
		display: inline-block;
		width: 50%;
		text-align: right;
	}

`
export const AircraftsList = styled.ul`
	max-height: 100%;
	list-style: none;
	margin: 0;
	padding: 0;
`;