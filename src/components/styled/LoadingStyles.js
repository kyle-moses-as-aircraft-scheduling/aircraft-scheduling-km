
import styled, { keyframes } from "styled-components";

export const BounceAnimation = keyframes`
	0% { margin-left: 0; }
	40% { margin-left: 20px }
	50% { margin-left: 20px }
	60% { margin-left: 20px }
  	100% { margin-left: 0 }
`;

export const DotWrapper = styled.div`
  display: block;
  margin-left: 50%;
  transform: translate(-55%, 0);
  width: 100%;
`;

export const Dot = styled.div`
	display: inline-block;
  background-color: #333;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  margin: 0 5px;
  /* Animation */
  animation: ${BounceAnimation} 0.6s linear infinite;
  animation-delay: ${props => props.delay};
`;