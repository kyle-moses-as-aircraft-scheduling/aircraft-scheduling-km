import { useState, useEffect } from 'react';
import { FlightScheduler, Header } from './components/styled/FlightScheduler';
import { calculateBookPercent } from './lib/utils';
import Aircrafts from './components/Aircrafts';
import Flights from './components/Flights';
import Rotation from './components/Rotation';
import { fetchData } from './lib/fetchData';
import { ExclamationSquareFill } from '@styled-icons/bootstrap';

const App = () => {
	// Set up states at the top level App with useStateHook
	const [aircraftsList, setAircraftsList] = useState([]);
	const [flightList, setFlightList] = useState([]);
	const [rotationList, setRotationList] = useState([]);
	const [currentRotation, setCurrentRotation] = useState([])


	// Set states with useEffect hook
	useEffect(()=> {
		const getAircrafts = async () => {
			const aircraftsRes = await fetchData('https://infinite-dawn-93085.herokuapp.com/aircrafts');
			setAircraftsList(aircraftsRes.data)
		}
		const getFlights = async () => {
			const flightsRes = await fetchData('https://infinite-dawn-93085.herokuapp.com/flights?limit=9000');
			setFlightList(flightsRes.data)
		}
		getAircrafts();
		getFlights();
	}, []);

	// Update State Methods passed to componnents through props
	const addFlight = (id) => {
		// Match the flight to add from the flight list
		const flightToAdd = flightList.filter((flight)=> flight.id === id);
		const flightToAddTime = flightToAdd[0].arrivaltime - flightToAdd[0].departuretime;

		if(calculateBookPercent(rotationList, flightToAdd[0]) > 100){
			alert('Additional Flight ' + flightToAdd[0].id +'\'s flight time('+ flightToAddTime +' seconds) is too much! Aircraft must be grounded by midnight! ');
		} else if (flightToAdd[0].departuretime - (currentRotation && currentRotation.arrivaltime + 1200) < 0) {
			alert('Flight' + flightToAdd[0].id +' depature time is before the latest flights arrival time');
		} else {
			// add it to the current rotation
			setRotationList([...rotationList, flightToAdd[0]]) 
			// set current rotation for destination matching
			setCurrentRotation(flightToAdd[0]);
			// update the flight list to remove the flight rotation
			setFlightList(flightList.filter((flight)=> flight.id !== id));
		}
	}
	const removeFlight = (id) => {
		// Match the flight to remove from the rotation list
		const flightToRemove = rotationList.filter((flight) => flight.id === id)[0];
		// Filter the array down to everything exccept the matching flight
		setRotationList(rotationList.filter((flight) => flight.id !== flightToRemove.id));
		// Get the last item in the roation list array to set the stage for adding new flights
		const [lastRotation] = rotationList.filter((flight) => flight.id !== flightToRemove.id).slice(-1);
		// set the current rotation to the last item in the array or undefined if it's empty
		setCurrentRotation(rotationList.length > 1 ? lastRotation : undefined);
		// add the removed rotation back into the flight list, then sort
		setFlightList([flightToRemove, ...flightList].sort((a,b)=> a.id < b.id ? -1 : 1));
	}
	// get todays date
	let today = new Date(),
		dd = String(today.getDate()).padStart(2, '0'),
		mm = String(today.getMonth() + 1).padStart(2, '0'),
		yyyy = today.getFullYear();
	
	today = mm + '/' + dd + '/' + yyyy;

	return (
		<>
			<Header>Today: {today}</Header>
			<FlightScheduler>
				<Aircrafts aircraftsList={aircraftsList} rotationList={rotationList}/>
				<Rotation rotationList={rotationList} removeFlight={removeFlight}/>
				<Flights flightList={flightList} addFlight={addFlight} currentDestination={currentRotation && currentRotation.destination}/>
			</FlightScheduler>
		</>
	);
}

export default App;
