export const calculateBookPercent = (rotationList, flightToAdd) => {
	let totalTime = 0;	
	rotationList.forEach((flight, index, flights) => {

		// add flight time
		totalTime = totalTime + parseInt((flight.arrivaltime - flight.departuretime));
		// add idle time from previous flight
		if(index >= 1) {
			let previousFlight = flights[index-1];
			totalTime += parseInt((flight.departuretime - previousFlight.arrivaltime));
		}
		// add turn around time
		totalTime += parseInt(1200);

	});
	if (flightToAdd){
		// AdditionalTime 
		totalTime = totalTime + parseInt((flightToAdd.arrivaltime - flightToAdd.departuretime));
	}
	// total seconds devicded by 24 hours
	totalTime = parseInt((totalTime / 86400)*100);

	return totalTime.toFixed(2);
}
