import { fetchData } from '../../lib/fetchData';

describe('fetchData', () => {
	let mockUrl;
	let mockResp;

	beforeEach(()=>{
		mockUrl = 'https://infinite-dawn-93085.herokuapp.com/aircrafts'
		mockResp = [{"ident":"GABCD","type":"A320","economySeats":186,"base":"EGKK"}];

		window.fetch = jest.fn().mockImplementation(()=>{
			return Promise.resolve({
				ok: true,
				json: () => Promise.resolve(mockResp)
			});
		});
	});

	it('should call fetch with params', () => {
		fetchData(mockUrl, {});
		expect(window.fetch).toHaveBeenCalledWith(mockUrl, {});
	});

	it('should return a response if ok', async () => {
		let resp = await fetchData(mockUrl, {});
		expect(resp).toEqual(mockResp);
	});

	it('should return an error response if not ok', async () => {
		window.fetch = jest.fn().mockImplementation(()=>{
			return Promise.resolve({
				ok: false
			});
		});
		await expect(fetchData()).rejects.toEqual(Error());
	});
});