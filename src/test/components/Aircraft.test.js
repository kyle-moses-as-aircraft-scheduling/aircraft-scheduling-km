import { render, screen, cleanup } from '@testing-library/react';
import renderer from 'react-test-renderer';
import Aircraft from '../../components/Aircraft';

describe('Aircraft', () => {
	afterEach(()=>{
		cleanup();
	})
	test('should render elements',  () => {
		render(<Aircraft ident={"ABCDEFG"} bookPercent={"50"} />);
		const aircraftElement = screen.getByText(/ABCDEFG/);
		expect(aircraftElement).toBeInTheDocument();
	});
	test('matches snapshot', () => {
		const tree = renderer.create(<Aircraft />).toJSON();
		expect(tree).toMatchSnapshot();
	});
})
