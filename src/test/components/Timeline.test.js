

import { cleanup } from '@testing-library/react';
import Enzyme, { mount,shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import Timeline from '../../components/Timeline';

const fakeflightList=[{
		id: "AS1001",
		departuretime: 10000,
		arrivaltime: 30000,
		readable_departure: "06:00",
		readable_arrival: "07:15",
		origin: "LFSB",
		destination: "LFMN"
	}];

describe('Timeline', () => {
	beforeEach(()=>{
		Enzyme.configure({ adapter: new Adapter() });
	});
	afterEach(()=>{
		cleanup();
	});
	test('should render TimeSlots with a positive timeSlotWidth',  () => {
		const wrapper = shallow(<Timeline rotationList={fakeflightList}/>);
		expect(wrapper.find('Timeslot').prop('timeSlotWidth')).toBeGreaterThan(0);
	});
	test('matches snapshot', () => {
		const wrapper = shallow(<Timeline rotationList={fakeflightList}/>);
		expect(wrapper).toMatchSnapshot();
	});
});
