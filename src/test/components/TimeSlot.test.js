import { cleanup } from '@testing-library/react';
import Enzyme, { mount,shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import TimeSlot from '../../components/TimeSlot';

describe('TimeSlot', () => {
	beforeEach(()=>{
		Enzyme.configure({ adapter: new Adapter() });
	});
	afterEach(()=>{
		cleanup();
	});
	test('matches snapshot', () => {
		const wrapper = shallow(<TimeSlot timeSlotWIdth={30} />);
		expect(wrapper).toMatchSnapshot();
	});
});
