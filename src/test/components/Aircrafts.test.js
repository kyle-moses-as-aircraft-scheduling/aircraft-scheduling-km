import { screen, cleanup, render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import Aircrafts from '../../components/Aircrafts';


describe('Aircrafts', () => {
	afterEach(()=>{
		cleanup();
	})
	test('should render elements',  () => {
		render(<Aircrafts />);
		const AircraftsHeaderElement = screen.getByText(/Aircrafts/);
		expect(AircraftsHeaderElement).toBeInTheDocument();
	});
	test('matches snapshot', () => {
		const tree = renderer.create(<Aircrafts />).toJSON();
		expect(tree).toMatchSnapshot();
	});
})