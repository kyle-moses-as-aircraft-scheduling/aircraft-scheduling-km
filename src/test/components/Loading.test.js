

import { cleanup } from '@testing-library/react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import Loading from '../../components/Loading';


describe('Loading', () => {
	beforeEach(()=>{
		Enzyme.configure({ adapter: new Adapter() });
	});
	afterEach(()=>{
		cleanup();
	});
	test('matches snapshot', () => {
		const wrapper = shallow(<Loading />);
		expect(wrapper).toMatchSnapshot();
	});
})
