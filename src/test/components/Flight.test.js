import {  render, screen, cleanup } from '@testing-library/react';
import Enzyme, { mount, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import renderer from 'react-test-renderer';
import Flight from '../../components/Flight';


describe('Flight', () => {
	beforeEach(()=>{
		Enzyme.configure({ adapter: new Adapter() });
	});
	afterEach(()=>{
		cleanup();
	});
	test('should render elements',  () => {
		
		const fakeProps = {
			flight: {
				id: "AS1001",
				departuretime: 21600,
				arrivaltime: 26100,
				readable_departure: "06:00",
				readable_arrival: "07:15",
				origin: "LFSB",
				destination: "LFMN"
			}, 
			isRotation: false, 
			addFlight: jest.fn(), 
			currentDestination: {
				id: "AS1001",
				departuretime: 21600,
				arrivaltime: 26100,
				readable_departure: "06:00",
				readable_arrival: "07:15",
				origin: "LFSB",
				destination: "LFMN"
			}, 
			removeFlight:  jest.fn()
		}
		const renderComponent = (fakeProps) => {
			return mount(<Flight {...fakeProps}/>);
		}
	});
	test('matches snapshot', () => {
		const { wrapper } = (fakeProps) => {return mount(<Flight {...fakeProps}/>)};
		expect(wrapper).toMatchSnapshot();
	});
})
