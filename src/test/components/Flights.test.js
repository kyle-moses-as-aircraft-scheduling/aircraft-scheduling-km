

import { cleanup } from '@testing-library/react';
import Enzyme, { mount } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import Flights from '../../components/Flights';

const fakeflightList=[{
		id: "AS1001",
		departuretime: 21600,
		arrivaltime: 26100,
		readable_departure: "06:00",
		readable_arrival: "07:15",
		origin: "LFSB",
		destination: "LFMN"
	}];

describe('Flights', () => {
	beforeEach(()=>{
		Enzyme.configure({ adapter: new Adapter() });
	});
	afterEach(()=>{
		cleanup();
	});
	test('should render elements',  () => {
		const wrapper = mount(<Flights flightList={fakeflightList}/>);
		expect(wrapper.find('p').text()).toEqual('AS1001');
	});
	test('matches snapshot', () => {
		const wrapper = mount(<Flights flightList={fakeflightList}/>);
		expect(wrapper).toMatchSnapshot();
	});
});
