

import { cleanup } from '@testing-library/react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16";
import Rotation from '../../components/Rotation';

const fakeflightList=[{
		id: "AS1001",
		departuretime: 21600,
		arrivaltime: 26100,
		readable_departure: "06:00",
		readable_arrival: "07:15",
		origin: "LFSB",
		destination: "LFMN"
	}];

describe('Rotation', () => {
	beforeEach(()=>{
		Enzyme.configure({ adapter: new Adapter() });
	});
	afterEach(()=>{
		cleanup();
	});
	test('should render flights',  () => {
		const wrapper = shallow(<Rotation rotationList={fakeflightList}/>);
		expect(wrapper.find('.rotationList')).not.toHaveLength(0);
	});
	test('matches snapshot', () => {
		const wrapper = shallow(<Rotation rotationList={fakeflightList}/>);
		expect(wrapper).toMatchSnapshot();
	});
});
