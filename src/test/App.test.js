import { render, screen, cleanup } from '@testing-library/react';
import renderer from 'react-test-renderer';
import App from '../App';

describe('App', () => {
	afterEach(()=>{
		cleanup();
	})
	test('should render elements',  () => {
		render(<App />);
		const dateHeaderElement = screen.getByText(/Today:/);
		const airCraftHeaderElement = screen.getByText(/Aircrafts/);
		const rotationHeaderElement = screen.getByText(/Rotation/);
		const flightsHeaderElement = screen.getByText(/Flights/);
		expect(dateHeaderElement).toBeInTheDocument();
		expect(airCraftHeaderElement).toBeInTheDocument();
		expect(rotationHeaderElement).toBeInTheDocument();
		expect(flightsHeaderElement).toBeInTheDocument();
	});
	test('matches snapshot', () => {
		const tree = renderer.create(<App />).toJSON();
		expect(tree).toMatchSnapshot();
	});
})
