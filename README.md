# AS's Aircraft Scheduler POC

## Assumptions
- The Flight Schduler POC is only for one day so no day switching functionality was built
- There is only one aircraft available so no aircraft selecting functionality was built
- Saving/schedule persistence was not built, page refresh will restore app to default

## Areas of improvement with more time available
- Styling and color choices could be better
- useState/effect and passing state methods down though props could be replaced with a state library like redux
- finding a better way to let the user know when flights don't line up time wise rather than using alerts, for example when the flight to be added to the rotation has a departure time that is before the latest flights arrival time(also taking into account turn around time); Currently making some calculations and firing an alert.

## Getting Started
- `git clone` the `main` branch
- `npm install`
- `npm run start`
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `npm test` Launches the test runner in the interactive watch mode.